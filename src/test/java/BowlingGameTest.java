import junit.framework.TestCase;

public class BowlingGameTest extends TestCase {
	private Game g;

	protected void setUp() throws Exception {
		g = new Game();
	}

	private void rollMany(int n, int pins) {
		for (int i = 0; i < n; i++)
			g.roll(pins);
	}

	public void testGutterGame() throws Exception {
		rollMany(20, 0);
		assertEquals(0, g.score());
	}

	public void testAllOnes() throws Exception {
		rollMany(20, 1);
		assertEquals(20, g.score());
	}

	public void testOneSpare() throws Exception {
		rollSpare();
		g.roll(3);
		rollMany(17, 0);
		assertEquals(16, g.score());
	}

	public void testOneStrike() throws Exception {
		rollStrike();
		g.roll(3);
		g.roll(4);
		rollMany(16, 0);
		assertEquals(24, g.score());
	}
	
	public void testStocazzo() throws Exception {
		g.roll(2);
		g.roll(8);
		g.roll(4);
		g.roll(4);
		g.roll(5);
		g.roll(3);
		g.roll(6);
		g.roll(4);
		g.roll(10);
		g.roll(10);
		g.roll(10);
		g.roll(8);
		g.roll(1);
		g.roll(3);
		g.roll(7);
		g.roll(8);
		g.roll(2);
		g.roll(10);
		assertEquals(174, g.score());
	}

	public void testPerfectGame() throws Exception {
		rollMany(12, 10);
		assertEquals(300, g.score());
	}

	public void testLastSpare() throws Exception {
		rollMany(9, 10);
		rollSpare();
		g.roll(10);
		assertEquals(275, g.score());
	}

	private void rollSpare() {
		g.roll(5);
		g.roll(5);
	}

	private void rollStrike() {
		g.roll(10);
	}

}
