import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Game {

	Frame[] frames = new Frame[10];
	ArrayList<Integer> sparesId = new ArrayList<Integer>();

	int numberFrame = 0;
	int numberRoll = 0;

	int FIRST_LAUNCH = 0;
	int SECOND_LAUNCH = 1;
	int nextLaunch = 0;
	Logger logger;

	public Game() {
		for (int i = 0; i < frames.length; i++) {
			frames[i] = new Frame();
			logger = LoggerFactory.getLogger(this.getClass());
		}
	}

	// tiro
	public void roll(int pins) {
		for (int i = 0; i < sparesId.size(); i++) {
			int id = sparesId.get(i);
			frames[id].extra += pins;
			sparesId.remove(i);
			if ((i + 1) < sparesId.size() && sparesId.get(i + 1) == id) {
				i++;
			}
		}

		if (numberFrame < frames.length) {
			if (nextLaunch == FIRST_LAUNCH) {
				frames[numberFrame].first = pins;
				nextLaunch = SECOND_LAUNCH;
				if (pins == 10) {
					nextLaunch = FIRST_LAUNCH;
					// strike
					sparesId.add(numberFrame);
					sparesId.add(numberFrame);
					numberFrame++;
				}
			} else if (nextLaunch == SECOND_LAUNCH) {
				frames[numberFrame].second = pins;
				nextLaunch = FIRST_LAUNCH;
				if (frames[numberFrame].isSpare()) {
					// spare
					sparesId.add(numberFrame);
				}
				numberFrame++;
			}
		}

		logger.info("birilli:" + pins);
		for (int i = 0; i < frames.length; i++) {
			logger.info("frame " + i + ": " + frames[i].total());
		}
		for (int i = 0; i < sparesId.size(); i++) {
			logger.info("spare: " + sparesId.get(i));
		}
		logger.info("********************************************");
	}

	// punteggio
	public int score() {
		int result = 0;
		for (int i = 0; i < frames.length; i++) {
			result += frames[i].total();
		}
		return result;
	}

}
