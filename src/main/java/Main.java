import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Main {

	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(Game.class);
		Logger loggerP1 = LoggerFactory.getLogger(Player.class);
		Logger loggerP2 = LoggerFactory.getLogger(Player.class);
		
		logger.info("Hello World");

		Player p1 = new Player(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);
		Player p2 = new Player(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);

		for (int i = 0; i < 10; i++) {
			loggerP1.info("Player1 rolls:");
			p1.roll(i);
			loggerP2.info("Player2 rolls:");
			p2.roll(i);
		}

		//System.out.println("player1 score: " + p1.score());
		logger.info("player1 score: " + p1.score());
		logger.info("player2 score: " + p2.score());
	}
}