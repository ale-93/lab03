public class Frame {
	public int first = 0;
	public int second = 0;
	public int extra = 0;

	public boolean isSpare() {
		if ((first + second) == 10 && !isStrike()) {
			return true;
		}
		return false;
	}

	public boolean isStrike() {
		if (first == 10) {
			return true;
		}
		return false;
	}

	public int total() {
		return first + second + extra;
	}
}