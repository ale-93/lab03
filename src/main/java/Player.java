import java.util.ArrayList;

public class Player {

	ArrayList<Integer> pinsArray = new ArrayList<Integer>();
	Game g;

	public Player(int... allPins) {
		for (int pins : allPins) {
			pinsArray.add(pins);
		}
		g = new Game();
	}

	public void roll(int rollNumber) {
		if (rollNumber < 9) {
			int pins = pinsArray.get(0);
			pinsArray.remove(0);
			g.roll(pins);
			if (pins == 10) {
				return;
			}
			pins = pinsArray.get(0);
			pinsArray.remove(0);
			g.roll(pins);
		} else {
			while (!pinsArray.isEmpty()) {
				int pins = pinsArray.get(0);
				pinsArray.remove(0);
				g.roll(pins);
			}
		}
	}

	public int score() {
		return g.score();
	}
}
